<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Score::class, function (Faker $faker) {
    return [
        'student_id' => $faker->unique()->numberBetween($min =1, $max =150),
        'course_id' => 3,
        'score' => rand(2,5)
    ];
});
