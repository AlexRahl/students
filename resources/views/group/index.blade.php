@extends('layouts.app')

@section('group.index')
    @php $wells = collect(); @endphp
    <div class="container-fluid">
        @foreach ($groups as $group)
            {{ $group->name }} 
            <table class="table table-sm table-bordered table-hover">
                <thead>
                    <tr>
                      <th>ФИО</th>
                      <th>Русский язык</th>
                      <th>Математика</th>
                      <th>История</th>
                      <th>Средний балл</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($group->student as $student)
                        @php
                            $result = $student->getAvgScore();

                            if ($result >= 4.5) $wells->push($student);

                            if ($result == 5)
                                $color = 'success';
                            elseif ($result >= 4.5)
                                $color = 'warning';
                            elseif ($result <= 3)
                                $color = 'danger';
                            else
                                $color = '';
                        @endphp
                        <tr @if (!empty($color)) class="table-{{ $color }}" @endif>
                            <td>{{ $student->name }}</td>
                            @foreach ($student->score as $score)
                                <td>{{ $score->score }}</td>
                            @endforeach
                            <td>{{ $result }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @endforeach
        <div class="container-fluid">
            <table class="table table-sm table-bordered table-hover" style="margin-top: 10px">
                <thead>
                    <tr>
                        <th>Группа</th>
                        <th>ФИО</th>
                        <th>Средний балл</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($wells as $student)
                        @php 
                            $result = $student->getAvgScore();
                        @endphp
                            <tr class="table-{{ $result == 5 ? 'success' : ($result >= 4.5 ? 'warning' : '') }}">
                                <th>{{ $student->group->name }}</th>
                                <th>{{ $student->name }}</th>
                                <th>{{ $result }}</th>
                            </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection