@extends('layouts.app')

@section('student.index')

@include('student.filter')
    <div class="container-fluid">
        <div class="col-md-10">
            <form action="{{ route('students.filter', ['sort' => request('sort'), 'filter' => request('filter') ]) }}" method="post" class="form-control">
                {{ csrf_field()}}
                <label>Имя студента:</label> <input type="text" name="filter[name]" value="{{ $filter['name'] OR null }}" />
                <input type="submit" name="" value="Filter">
                <label>ИД Студента:</label> <input type="text" name="filter[id]" value="{{ $filter['id'] OR null }}" />
                <input type="submit" name="" value="Filter">
            </form>
        </div>
        <div clas="col-md-10">
            <table class="table table-sm table-bordered table-hover">
                <thead>
                    <tr>
                        <th>
                            Группа
                            <a href="{{ route('students.filter', ['sort[column]' => 'group_id', 'sort[value]' => 'asc', 'filter' => request('filter')]) }}">
                            <span class="badge {{ ($sort['column'] == 'group_id' && $sort['value'] == 'asc') ? 'badge-danger' : 'badge-secondary' }}">▲</span></a>
                            <a href="{{ route('students.filter', ['sort[column]' => 'group_id', 'sort[value]' => 'desc', 'filter' => request('filter')]) }}">
                            <span class="badge {{ ($sort['column'] == 'group_id' && $sort['value'] == 'desc') ? 'badge-danger' : 'badge-secondary' }}">▼</span></a>
                        </th>
                        <th>
                            Cтудент ID
                            <a href="{{ route('students.filter', ['sort[column]' => 'id', 'sort[value]' => 'asc', 'filter' => request('filter')]) }}">
                            <span class="badge {{ ($sort['column'] == 'id' && $sort['value'] == 'asc') ? 'badge-danger' : 'badge-secondary' }}">▲</span></a>
                            <a href="{{ route('students.filter', ['sort[column]' => 'id', 'sort[value]' => 'desc', 'filter' => request('filter')]) }}">
                            <span class="badge {{ ($sort['column'] == 'id' && $sort['value'] == 'desc') ? 'badge-danger' : 'badge-secondary' }}">▼</span></a>
                        </th>
                        <th>
                            ФИО
                            <a href="{{ route('students.filter', ['sort[column]' => 'name', 'sort[value]' => 'asc', 'filter' => request('filter')]) }}">
                            <span class="badge {{ ($sort['column'] == 'name' && $sort['value'] == 'asc') ? 'badge-danger' : 'badge-secondary' }}">▲</span></a>
                            <a href="{{ route('students.filter', ['sort[column]' => 'name', 'sort[value]' => 'desc', 'filter' => request('filter')]) }}">
                            <span class="badge {{ ($sort['column'] == 'name' && $sort['value'] == 'desc') ? 'badge-danger' : 'badge-secondary' }}">▼</span></a>
                        </th>
                    </tr>
                </thead>
                @foreach ($students as $student)
                <tbody>
                    <tr>
                        <td>{{ $student->group->name }}</td>
                        <td>{{ $student->id }}</td>
                        <td>{{ $student->name }}</td>
                        <td><a class="btn btn-sm btn-outline-success" href="{{ route('students.show', $student->id )}}">Профиль</a></td>
                    </tr>
                </tbody>
                @endforeach
            </table>
            {{ $students->links('common.links') }}
        </div>
    </div>
@endsection