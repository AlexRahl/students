@section('student.filter')
    <div class="card card-default">
        <div class="card-body">
            <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ (isset($filter['group_id'])) ? $groups->where('id', $filter['group_id'])->first()->name : 'Фильтр групп' }}
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                @foreach ($groups as $group)
                    <a class="dropdown-item" href="{{ route('students.filter', ['filter[group_id]' => $group->id, 'filter[name]' => $name, 'sort' => request('sort')]) }}">{{ $group->name }}</a>
                @endforeach
                <a class="dropdown-item" href="/students">Все группы</a>
                </div>
            </div>
        </div>
    </div>
@endsection