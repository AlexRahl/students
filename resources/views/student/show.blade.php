@extends('layouts.app')

@section('student.show')

<div class="row">
    <div class="card card-default">
        <div class="card-body">
        @if ($student->avatar_path != null)
            <img src="{{ $student->avatar_path }}">
        @else
            <img src="/images/avatars/default_avatar.jpg">
        @endif
            <form action="{{ route('students.show', [$student->id] )}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
                <input type="file" name="avatar"> <br>
                <button type="submit">Изменить аватар</button>
            </form>
            <form action="{{ route('students.delete', $student->id) }}" method="post">
            {{csrf_field()}}
            {{method_field('DELETE')}}
            <button type="submit">Удалить аватар</button>
        </div>
    </div>
    <div class="card card-default">
        <div class="card-body">
            <form action="{{ route('students.show', [$student->id] )}}" method="post" class="form-control">
            {{ csrf_field() }}
                <label>ФИО</label> <input type="text" name="name" value="{{ $student->name }}"> <br>
                <label>Группа</label> <input type="text" name="group_id" value="{{ $student->group_id }}"> <br>
                <label>Оценки:</label> <br>
                <label>Русский язык</label> <input type="text" name="score-1" value="{{ $student->score->where('course_id',1)->first()->score or null}}"> <br> 
                <label>Математика</label> <input type="text" name="score-2" value="{{ $student->score->where('course_id',2)->first()->score or null}}"> <br>
                <label>История</label> <input type="text" name="score-3" value="{{ $student->score->where('course_id',3)->first()->score or null}}"> <br>
                <input type="submit" name="" value="Изменить профиль">
            </form>
        </div>
    </div>
</div>
<form action="{{ route('students.destroy', $student->id) }}" method="post">
{{csrf_field()}}
{{method_field('DELETE')}}
<button type="submit">Удалить профиль</button>
</form>

@endsection