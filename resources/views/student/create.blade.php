@extends('layouts.app')

@section('student.create')

<div class="card card-default">
    <div class="card-body">
        <form action="{{ route('students.create') }}" method="post" class="form-control">
        {{ csrf_field() }}
            <label>ФИО</label> <input type="text" name="name" placeholder="Введите ФИО"> <br>
            <label>Группа</label> <input type="text" name="group_id" placeholder="Укажите номер группы"> <br>
            <label>Оценки:</label> <br>
            <label>Русский язык</label> <input type="text" name="score-1" value =""> <br> 
            <label>Математика</label> <input type="text" name="score-2" value =""> <br>
            <label>История</label> <input type="text" name="score-3" value =""> <br>
            <input type="submit" name="" value="Создать профиль">
        </form>
    </div>
</div>

@endsection