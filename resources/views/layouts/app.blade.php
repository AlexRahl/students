@extends('base')

@section('nav')
    @include('common.navbar')
@endsection

@section('content')
    <div class="row">
        <div class="container-fluid col-2">
            @yield('student.filter')
        </div>

        <div class="container-fluid col-10">
            @yield('student.index')
        </div>

        <div class="container-fluid">
            @yield('group.index')
        </div>

        <div class="container-fluid">
            @yield('student.show')
        </div>

        <div class="container-fluid">
            @yield('student.create')
        </div>
    </div>
@endsection