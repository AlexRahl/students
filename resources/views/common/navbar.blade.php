<nav class="navbar navbar-expand-sm navbar-dark bg-dark">
    <a class="navbar-brand" href="/">Harvard University</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">
                <a class="nav-link" href="{{ route('groups.index') }}">Группы</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="{{ route('students.index') }}">Студенты</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="{{ route('students.create') }}">Добавить профиль студента</a>
            </li>
        </ul>
    </div>
</nav>