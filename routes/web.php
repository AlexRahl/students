<?php

Route::get('/', 'GroupController@index')->name('groups.index');

Route::get('students', 'StudentController@index')->name('students.index');
Route::any('students/filter', 'StudentController@filter')->name('students.filter');
Route::get('students/show/{student}', 'StudentController@show')->name('students.show');
Route::post('students/show/{student}', 'StudentController@edit')->name('students.edit');
Route::get('students/create/', 'StudentController@create')->name('students.create');
Route::post('students/create/', 'StudentController@store')->name('students.store');
Route::delete('students/destroy/{student}', 'StudentController@destroy')->name('students.destroy');

Route::post('students/show/{student}', 'StudentController@upload')->name('students.upload');
Route::delete('students/show/{student}', 'StudentController@delete')->name('students.delete');