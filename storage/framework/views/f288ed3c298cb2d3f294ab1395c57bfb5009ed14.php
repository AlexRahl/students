<?php $__env->startSection('filter'); ?>
    <div class="card card-default">
        <div class="card-body">
            <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?php echo e((isset($filter['group_id'])) ? $groups->where('id', $filter['group_id'])->first()->name : 'Фильтр групп'); ?>

                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <?php $__currentLoopData = $groups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $group): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <a class="dropdown-item" href="<?php echo e(route('students.filter', ['filter[group_id]' => $group->id, 'filter[name]' => $name, 'sort' => request('sort')])); ?>"><?php echo e($group->name); ?></a>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <a class="dropdown-item" href="/students">Все группы</a>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>