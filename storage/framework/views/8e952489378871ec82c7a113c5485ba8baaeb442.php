<?php $__env->startSection('index'); ?>
    <?php $wells = collect(); ?>
    <div class="container-fluid">
        <?php $__currentLoopData = $groups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $group): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php echo e($group->name); ?> 
            <table class="table table-sm table-bordered table-hover">
                <thead>
                    <tr>
                      <th>ФИО</th>
                      <th>Русский язык</th>
                      <th>Математика</th>
                      <th>История</th>
                      <th>Средний балл</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $group->student; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $student): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php
                            $result = $student->getAvgScore();

                            if ($result >= 4.5) $wells->push($student);

                            if ($result == 5)
                                $color = 'success';
                            elseif ($result >= 4.5)
                                $color = 'warning';
                            elseif ($result <= 3)
                                $color = 'danger';
                            else
                                $color = '';
                        ?>
                        <tr <?php if(!empty($color)): ?> class="table-<?php echo e($color); ?>" <?php endif; ?>>
                            <td><?php echo e($student->name); ?></td>
                            <?php $__currentLoopData = $student->score; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $score): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td><?php echo e($score->score); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <td><?php echo e($result); ?></td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <div class="container-fluid">
            <table class="table table-sm table-bordered table-hover" style="margin-top: 10px">
                <thead>
                    <tr>
                        <th>Группа</th>
                        <th>ФИО</th>
                        <th>Средний балл</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $wells; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $student): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php 
                            $result = $student->getAvgScore();
                        ?>
                            <tr class="table-<?php echo e($result == 5 ? 'success' : ($result >= 4.5 ? 'warning' : '')); ?>">
                                <th><?php echo e($student->group->name); ?></th>
                                <th><?php echo e($student->name); ?></th>
                                <th><?php echo e($result); ?></th>
                            </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>