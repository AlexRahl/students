<?php $__env->startSection('students'); ?>

<?php echo $__env->make('filter.student', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="container-fluid">

        <div class="col-md-10">

            <form action="<?php echo e(route('students.filter', ['sort' => request('sort'), 'filter' => request('filter') ])); ?>" method="post" class="form-control">
                <?php echo e(csrf_field()); ?>

                <label>Имя студента:</label> <input type="text" name="filter[name]" value="<?php echo e(isset($filter['name']) ? $filter['name'] : null); ?>" />
                <input type="submit" name="" value="Filter">
                <label>ИД Студента:</label> <input type="text" name="filter[id]" value="<?php echo e(isset($filter['id']) ? $filter['id'] : null); ?>" />
                <input type="submit" name="" value="Filter">
            </form>

        </div>

        <div clas="col-md-10">

            <table class="table table-sm table-bordered table-hover">
                <thead>
                    <tr>
                        <th>
                            Группа
                            <a href="<?php echo e(route('students.filter', ['sort[column]' => 'group_id', 'sort[value]' => 'asc','page' => request('page'), 'filter' => request('filter')])); ?>">
                            <span class="badge <?php echo e(($sort['column'] == 'group_id' && $sort['value'] == 'asc') ? 'badge-danger' : 'badge-secondary'); ?>">▲</span></a>
                            <a href="<?php echo e(route('students.filter', ['sort[column]' => 'group_id', 'sort[value]' => 'desc','page' => request('page'), 'filter' => request('filter')])); ?>">
                            <span class="badge <?php echo e(($sort['column'] == 'group_id' && $sort['value'] == 'desc') ? 'badge-danger' : 'badge-secondary'); ?>">▼</span></a>
                        </th>
                        <th>
                            Cтудент ID
                            <a href="<?php echo e(route('students.filter', ['sort[column]' => 'id', 'sort[value]' => 'asc','page' => request('page'), 'filter' => request('filter')])); ?>">
                            <span class="badge <?php echo e(($sort['column'] == 'id' && $sort['value'] == 'asc') ? 'badge-danger' : 'badge-secondary'); ?>">▲</span></a>
                            <a href="<?php echo e(route('students.filter', ['sort[column]' => 'id', 'sort[value]' => 'desc','page' => request('page'), 'filter' => request('filter')])); ?>">
                            <span class="badge <?php echo e(($sort['column'] == 'id' && $sort['value'] == 'desc') ? 'badge-danger' : 'badge-secondary'); ?>">▼</span></a>
                        </th>
                        <th>
                            ФИО
                            <a href="<?php echo e(route('students.filter', ['sort[column]' => 'name', 'sort[value]' => 'asc','page' => request('page'), 'filter' => request('filter')])); ?>">
                            <span class="badge <?php echo e(($sort['column'] == 'name' && $sort['value'] == 'asc') ? 'badge-danger' : 'badge-secondary'); ?>">▲</span></a>
                            <a href="<?php echo e(route('students.filter', ['sort[column]' => 'name', 'sort[value]' => 'desc','page' => request('page'), 'filter' => request('filter')])); ?>">
                            <span class="badge <?php echo e(($sort['column'] == 'name' && $sort['value'] == 'desc') ? 'badge-danger' : 'badge-secondary'); ?>">▼</span></a>
                        </th>
                    </tr>
                </thead>

                <?php $__currentLoopData = $students; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $student): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tbody>
                    <tr>
                        <td><?php echo e($student->group->name); ?></td>
                        <td><?php echo e($student->id); ?></td>
                        <td><?php echo e($student->name); ?></td>
                    </tr>
                </tbody>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            </table>

            <?php echo e($students->links('common.links')); ?>


        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>