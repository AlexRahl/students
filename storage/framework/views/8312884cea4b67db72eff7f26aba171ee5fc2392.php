<?php $__env->startSection('studentsfilter'); ?>
<div class="container-fluid" style="margin-top: 10px">
    <div class="card">
        <div class="card-header">
            Фильтры
        </div>
        <div class="class-body">
            <div class="row">
                <div class="container-fluid col-1">
                    <div class="btn-group">
                        <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Action
                        </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                    </div>
                </div>
                <div class="container-fluid col-1">
                    <div class="btn-group">
                        <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Action
                        </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                    </div>
                </div>
                <div class="container-fluid col-3">
                    <div class="btn-group" role="group" aria-label="Button group dropdown">
                        <div class="btn-group" role="group">
                    <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Русский язык
                    </button>
                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                <a class="dropdown-item" href="#">2</a>
                                <a class="dropdown-item" href="#">3</a>
                                <a class="dropdown-item" href="#">4</a>
                                <a class="dropdown-item" href="#">5</a>
                            </div>
                        </div>
                        <div class="btn-group" role="group">
                    <button id="btnGroupDrop2" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Математика
                    </button>
                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop2">
                                <a class="dropdown-item" href="#">2</a>
                                <a class="dropdown-item" href="#">3</a>
                                <a class="dropdown-item" href="#">4</a>
                                <a class="dropdown-item" href="#">5</a>
                            </div>
                        </div>
                        <div class="btn-group" role="group">
                    <button id="btnGroupDrop3" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    История
                    </button>
                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop3">
                                <a class="dropdown-item" href="#">2</a>
                                <a class="dropdown-item" href="#">3</a>
                                <a class="dropdown-item" href="#">4</a>
                                <a class="dropdown-item" href="#">5</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>