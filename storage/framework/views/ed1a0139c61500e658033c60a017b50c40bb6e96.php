<?php $__env->startSection('nav'); ?>
    <?php echo $__env->make('common.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="container-fluid col-2">
            <?php echo $__env->yieldContent('student.filter'); ?>
        </div>

        <div class="container-fluid col-10">
            <?php echo $__env->yieldContent('student.index'); ?>
        </div>

        <div class="container-fluid">
            <?php echo $__env->yieldContent('group.index'); ?>
        </div>

        <div class="container-fluid">
            <?php echo $__env->yieldContent('student.show'); ?>
        </div>

        <div class="container-fluid">
            <?php echo $__env->yieldContent('student.create'); ?>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('base', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>