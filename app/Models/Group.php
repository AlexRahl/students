<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model 
{
    protected $table = 'group';

    protected $fillable = ['name'];

    public $timestamps = false;

    public function student()
    {
        return $this->hasMany(Student::class);
    }
}
