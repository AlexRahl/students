<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'course';

    protected $fillable = ['name'];

    public $timestamps = false;

    public function score()
    {
        return $this->hasMany(Score::class);
    }
}
