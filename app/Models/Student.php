<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model 
{
    protected $table = 'student';

    protected $fillable = ['name', 'group_id'];

    public $timestamps = false;

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function score()
    {
        return $this->hasMany(Score::class);
    }

    public function scopeFilter($qb, $filter)
    {
        if (isset($filter['group_id'])) $qb->where('group_id', '=', $filter['group_id']);

        if (isset($filter['name'])) $qb->where('name', 'LIKE', "%{$filter['name']}%");

        if (isset($filter['id'])) $qb->where('id', '=', $filter['id']);
    }

    public function scopeSort($qb, $sort)
    {
        if (isset($sort)) $qb->orderBy($sort['column'], $sort['value']);
    }

    public function getAvgScore()
    {
        return round($this->score->sum('score') / 3, 1);
    }
}
    
