<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Models\Group;
use App\Models\Score;
use App\Models\Course;
use App\Models\Student;

class StudentController extends Controller
{
    public function index(Request $request)
    {    
        $filter = $request->input('filter');
        $sort = $request->input('sort');
        
        if (isset($filter['name']))
            $name = $filter['name'];
        else
            $name = null;

        $students = Student::filter($request->input('filter'))
            ->sort($request->input('sort'))
            ->with('group', 'score.course')
            ->paginate(10)
            ->appends(['sort' => $sort,'filter' => $filter]);;
        
        $groups = Group::all();

        return view('student.index', compact ('students', 'groups', 'name', 'sort', 'filter')); 
    }

    public function filter(Request $request)
    {
        $filter = $request->input('filter');

        if ($request->get('filter'))
            $filter = array_merge($request->get('filter'), $request->input('filter'));

        return redirect()->route('students.index', [
            'filter' => $filter,
            'sort' => $request->input('sort')
        ]);
     
    }

    public function show(Student $student)
    {
        return view('student.show', compact ('student'));
    }

    public function create()
    {
        return view('student.create');
    }

    public function store(Request $request)
    {
        $new = Student::create([
            'name'=> $request->input('name'),
            'group_id' => $request->input('group_id')
            ]);

        for ($i = 1; $i < 4; $i++) {
        Score::create([
            'student_id' => $new->id,
            'course_id' => $i,
            'score' => $request->input('score-'.$i)
            ]);
        }

        return redirect()->route('students.index');
    }

    public function edit(Request $request, Student $student)
    {
        $student->name = $request->input('name');
        $student->group_id = $request->input('group_id');
        $student->save();

        for ($i = 1; $i < 4; $i++) {
        Score::where('student_id', $student->id)->where('course_id', $i)->update([
            'student_id' => $student->id,
            'course_id' => $i,
            'score' => $request->input('score-'.$i)
            ]);
        }

        return redirect()->route('students.index');
    }

    public function destroy(Student $student)
    {
        if ($student->avatar_path != null) {
            if (file_exists(public_path().$student->avatar_path)) {
                unlink(public_path().$student->avatar_path);
            }
        }

        $student->delete();
        
        return redirect()->route('students.index');
    }

    public function upload(Request $request, Student $student)
    {
        if ($request->hasFile('avatar')) {
            if ($student->avatar_path != null) {
                if (file_exists(public_path().$student->avatar_path)) {
                    unlink(public_path().$student->avatar_path);
                }
            }
       
            $file = $request->file('avatar');
            $filePath = '/images/avatars/'. time().'_'.$file->getClientOriginalName();
            $file->move(public_path('images/avatars'), time().'_'.$file->getClientOriginalName());

            $student->avatar_path = $filePath;
            $student->save();
        }

        return back();
    }

    public function delete(Student $student)
    {
        if ($student->avatar_path != null) {
            if (file_exists(public_path().$student->avatar_path)) {
                unlink(public_path().$student->avatar_path);
            }
        }

        $student->avatar_path = null;
        $student->save();

        return back();
    }
}
