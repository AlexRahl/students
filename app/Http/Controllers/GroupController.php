<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Group;
use App\Models\Score;
use App\Models\Course;
use App\Models\Student;

class GroupController extends Controller
{
    public function index()
    {   
        $groups = Group::with('student.score.course', 'student.group')->get();
        
        return view('group.index', compact('groups'));
    }
}

